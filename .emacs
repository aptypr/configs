(setq-default indent-tabs-mode nil)
					;(let ((default-directory "~/.emacs.d/lisp/"))
;  (normal-top-level-add-subdirs-to-load-path))
;(setq inferior-lisp-program "/usr/bin/sbcl")
;(add-to-list 'load-path "~/.emacs.d/lisp/")
;(require 'minimap)
;(add-to-list 'load-path "~/work/lisp/slime/")
;(require 'slime)
;(slime-setup)
;(require 'yasnippet)
(server-start)
(global-linum-mode 1)
(add-to-list 'load-path "~/Downloads/emacs-color-theme-solarized-master")
(require 'color-theme-solarized)
(color-theme-solarized-dark)
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (quote [f5]) 'revert-buffer)
(global-set-key (quote [f12]) 'shell)
(global-set-key (quote [f7]) 'compile)
(global-set-key [C-M-tab] 'buffer-menu)
(global-set-key [M-up] 'beginning-of-defun)
(global-set-key [M-down] 'end-of-defun)
(column-number-mode t)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(linum-mode t)
(customize-variable (quote tab-stop-list))
(setq c-basic-offset 8)
(setq indent-tabs-mode nil)
(setq tab-width 8)
(global-set-key "\C-l" 'goto-line)
;(set-face-attribute 'default nil :family "Anonymous Pro" :height 110)
(set-face-attribute 'default nil :font "Dejavu Sans Mono-10")
;(require 'gnuplot)
(setq auto-mode-alist
      (append '(("\\.\\(gp\\|gnuplot\\)$" . gnuplot-mode)) auto-mode-alist))
;(require 'git)
; switch between .h and .cpp by ctrl-tab, like a boss.
(add-hook 'c-mode-common-hook
  (lambda() 
    (local-set-key  (kbd "<C-tab>") 'ff-find-other-file)))
(add-hook 'c-mode-common-hook
  (lambda() 
    (local-set-key  (kbd "<C-tab>") 'ff-find-other-file)))
;(require 'color-theme)
;(color-theme-initialize)
;(require 'color-theme-solarized)
;(color-theme-solarized-dark)
;(color-theme-midnight)
(setq compilation-read-command nil) ;; no prompt for compilation!
(setq inhibit-startup-message t) ;; no startup message
;; mv config for indentation, and showing bad lines more then 80
(defun my-c-mode-hook ()
  (c-set-style "linux")
  (setq indent-tabs-mode t)
  (setq c-basic-offset 4)
  (set (make-local-variable 'whitespace-style)
       '(face trailing lines-tail space-before-tab space-before-tab::tab newline
              indentation empty space-after-tab space-after-tab::tab))
  ;(whitespace-mode 0)
  ;(whitespace-mode 1)
  (auto-fill-mode t)
  (c-toggle-auto-hungry-state -1))

(add-hook 'c-mode-hook 'my-c-mode-hook)
(put 'upcase-region 'disabled nil)

;(require 'quack)
;;; Always do syntax highlighting
(global-font-lock-mode 1)

;;; Also highlight parens
(setq show-paren-delay 0
      show-paren-style 'parenthesis)
(show-paren-mode 1)

;;; This is the binary name of my scheme implementation
(setq scheme-program-name "guile")
(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(quack-programs (quote ("guiel" "bigloo" "csi" "csi -hygienic" "gosh" "gsi" "gsi ~~/syntax-case.scm -" "guile" "kawa" "mit-scheme" "mred -z" "mzscheme" "mzscheme -il r6rs" "mzscheme -il typed-scheme" "mzscheme -M errortrace" "mzscheme3m" "mzschemecgc" "rs" "scheme" "scheme48" "scsh" "sisc" "stklos" "sxi"))))
(require 'outline)

(global-auto-revert-mode t)
(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))
;;(global-set-key [C-right] 'toggle-comment-on-line)
;;; This was installed by package-install.el.
;;; This provides support for the package system and
;;; interfacing with ELPA, the package archive.
;;; Move this code earlier if you want to reference
;;; packages in your .emacs.
;(when
;    (load
;     (expand-file-name "~/.emacs.d/elpa/package.el"))
;  (package-initialize))
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-c\C-k" 'kill-region)
(defun wrap-text (b e txt)
  "simple wrapper"
  (interactive "r\nMEnter text to wrap with: ")
  (save-restriction
    (narrow-to-region b e)
    (goto-char (point-min))
    (insert txt)
    (goto-char (point-max))
    (insert txt)
    ))
(global-set-key (kbd "C-x M-w") 'wrap-text)


